# README #

This repository is used for issue tracking only. For instructions on how to setup Spacedecode, please check out the [getting started guide](http://docs.spacedecode.com/getting-started).

### What is this repository for? ###

* Issue tracking
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution guidelines ###

* No pull-requests allowed;
* Simply create issues (Please avoid duplicates)

### Who do I talk to? ###

* Email future@spacedecode.com if you require direct communication.